package com.httpain.meatballs.admin;

import com.httpain.meatballs.feature.productimport.ProductImportService;
import com.httpain.meatballs.infrastructure.storage.LocationRepository;
import com.httpain.meatballs.model.Location;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

@Endpoint(id = "triggerProductImport")
@Component
public class ProductImportAdminTrigger {

    private final ProductImportService productImportService;
    private final LocationRepository locationRepository;

    public ProductImportAdminTrigger(ProductImportService productImportService,
                                     LocationRepository locationRepository) {
        this.productImportService = productImportService;
        this.locationRepository = locationRepository;
    }

    @WriteOperation
    public void triggerProductImport(@Selector long locationId) {
        Location location = locationRepository.findById(locationId);
        productImportService.parseNewProducts(location);
    }

}
