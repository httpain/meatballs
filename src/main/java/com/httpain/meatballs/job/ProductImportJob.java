package com.httpain.meatballs.job;

import com.httpain.meatballs.feature.productimport.ProductImportService;
import com.httpain.meatballs.infrastructure.storage.LocationRepository;
import org.springframework.stereotype.Component;

@Component
public class ProductImportJob {

    private final LocationRepository locationRepository;
    private final ProductImportService productImportService;

    public ProductImportJob(LocationRepository locationRepository, ProductImportService productImportService) {
        this.locationRepository = locationRepository;
        this.productImportService = productImportService;
    }

    public void foo() {
        locationRepository.findAll().forEach(productImportService::parseNewProducts);
    }
}
