package com.httpain.meatballs.infrastructure.web;

import com.httpain.meatballs.feature.subscription.SubscriptionService;
import com.httpain.meatballs.feature.subscription.SubscriptionService.SubscriptionType;
import com.httpain.meatballs.model.Subscription;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@Slf4j
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping("/subscribe")
    public String subscribe(@Validated @Valid @ModelAttribute Subscription subscription, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getFieldErrors()); //TODO
            return "redirect:/";
        }
        SubscriptionType subscriptionType = subscriptionService.sendConfirmation(subscription.getEmail(), subscription.getLocationId());

        if (subscriptionType == SubscriptionType.EXISTS) {
            return "redirect:/already_subscribed";
        } else {
            return "redirect:/subscribed";
        }
    }

    @GetMapping("/confirm")
    public String confirmSubscription(@RequestParam("i") String identity, @RequestParam("t") String token) {

        SubscriptionType subscriptionType = subscriptionService.confirmSubscription(identity, token);

        if (subscriptionType == SubscriptionType.EXISTS) {
            return "redirect:/already_subscribed";
        } else {
            return "redirect:/confirmed";
        }

    }

    @GetMapping("/unsubscribe")
    public String unsubscribe(@RequestParam("i") String identity, @RequestParam("t") String token) {
        subscriptionService.cancelSubscription(identity, token);
        return "redirect:/unsubscribed";
    }


    @GetMapping("/subscribed")
    public String subscribed() {
        return "subscribed";
    }

    @GetMapping("/already_subscribed")
    public String alreadySubscribed() {
        return "already_subscribed";
    }

    @GetMapping("/confirmed")
    public String confirmed() {
        return "confirmed";
    }


    @GetMapping("/unsubscribed")
    public String unsubscribed() {
        return "unsubscribed";
    }

}
