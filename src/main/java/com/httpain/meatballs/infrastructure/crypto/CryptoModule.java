package com.httpain.meatballs.infrastructure.crypto;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@Configuration
public class CryptoModule {

    @Bean
    @Qualifier("subscribeTokenEncoder")
    public PasswordEncoder subscribeTokenEncoder() {
        return newEncoder();
    }

    @Bean
    @Qualifier("unsubscribeTokenEncoder")
    public PasswordEncoder unsubscribeTokenEncoder() {
        return newEncoder();
    }

    @Bean
    @Qualifier("identityEncryptor")
    public TextEncryptor identityEncryptor() {
        return newEncryptor();
    }

    private PasswordEncoder newEncoder() {
        String secret = KeyGenerators.string().generateKey();
        return new Pbkdf2PasswordEncoder(secret);
    }

    private TextEncryptor newEncryptor() {
        String password = new Base64StringKeyGenerator(32).generateKey();
        String hexSalt = KeyGenerators.string().generateKey();
        return Encryptors.text(password, hexSalt);
    }
}
