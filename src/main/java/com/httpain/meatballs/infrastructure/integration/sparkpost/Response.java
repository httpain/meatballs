package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
class Response {
    @JsonCreator
    public Response(@JsonProperty("results") Results results) {
        this.results = results;
    }

    Results results;
}
