package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.httpain.meatballs.model.Email;
import com.httpain.meatballs.model.NewsletterSubscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SparkpostClient implements EmailSenderClient {

    private final String apiKey;
    private final String replyTo;
    private final SparkpostApi sparkpostApi;

    public SparkpostClient(@Value("${sparkpost.apiKey}") String apiKey,
                           @Value("${sparkpost.replyTo}") String replyTo,
                           SparkpostApi sparkpostApi) {
        this.apiKey = apiKey;
        this.replyTo = replyTo;
        this.sparkpostApi = sparkpostApi;
    }

    @Override
    public void sendTransactionalEmail(Email email, String recipientEmail) {
        Recipient recipient = new Recipient(new Address(recipientEmail), Collections.emptyMap());

        sendGenericEmail(email, Collections.singletonList(recipient));
    }

    @Override
    public void sendNewsletterEmail(Email email, List<NewsletterSubscriber> subscribers) {
        List<Recipient> recipients = subscribers.stream()
                .map(subscriber -> new Recipient(
                        new Address(subscriber.getEmail()),
                        Collections.singletonMap("unsubscribe_url", subscriber.getUnsubscribeLink())
                ))
                .collect(Collectors.toList());

        sendGenericEmail(email, recipients);
    }

    private void sendGenericEmail(Email email, List<Recipient> recipients) {
        Content content = new Content(email.getSender(), email.getSubject(), email.getHtmlContent(), replyTo);

        Transmission transmission = Transmission.builder()
                .options(Options.noTracking())
                .content(content)
                .recipients(recipients)
                .build();

        Response response = sparkpostApi.sendEmail(apiKey, transmission);
        if (response.getResults().getTotalRejectedRecipients() > 0) {
            log.warn("Rejected recipients in Sparkpost response: {}", response.toString());
        }
    }
}
