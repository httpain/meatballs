package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Map;

@Value
class Recipient {
    Address address;
    @JsonProperty("substitution_data")
    Map<String, Object> substitutionData;
}
