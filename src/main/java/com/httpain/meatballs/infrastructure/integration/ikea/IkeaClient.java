package com.httpain.meatballs.infrastructure.integration.ikea;

import com.httpain.meatballs.model.Location;
import com.httpain.meatballs.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class IkeaClient {

    public List<Product> parseLatestProducts(Location location) {
        String uri = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(location.getHostname())
                .path(location.getNewItemsPath())
                .build().toUriString();
        Document doc;
        try {
            doc = Jsoup.connect(uri).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Elements products = doc.select(".productContainer .parentContainer");

        log.info("Location '{}': imported {} products", location.getName(), products.size());

        return products.stream()
                .map(product -> {
                    String name = product.select(".prodName").text();
                    String description = product.select(".prodDesc").text();
                    String price = product.select(".prodPrice").text();
                    String url = product.select("a").attr("abs:href"); //absolute path
                    String imageUrl = product.select(".prodImg").attr("abs:src");
                    return new Product(name, description, price, url, imageUrl, location.getId());
                })
                .collect(Collectors.toList());
    }


}
