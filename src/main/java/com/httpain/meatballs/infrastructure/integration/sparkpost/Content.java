package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
class Content {
    String from;
    String subject;
    String html;
    @JsonProperty("reply_to")
    String replyTo;
}
