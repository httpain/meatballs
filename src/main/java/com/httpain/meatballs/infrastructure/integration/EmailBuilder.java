package com.httpain.meatballs.infrastructure.integration;

import com.httpain.meatballs.model.Email;
import com.httpain.meatballs.model.Product;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmailBuilder {
    private final String sender;
    private final PebbleEngine pebbleEngine;

    public EmailBuilder(@Value("${sparkpost.sender}") String sender,
                        PebbleEngine pebbleEngine) {
        this.sender = sender;
        this.pebbleEngine = pebbleEngine;
    }

    public Email buildConfirmationEmail(String locationName, String confirmationUrl) {
        String subject = "Please confirm your subscription";

        PebbleTemplate template = pebbleEngine.getTemplate("email/confirmation");

        Map<String, Object> context = new HashMap<>();
        context.put("locationName", locationName);
        context.put("confirmationUrl", confirmationUrl);

        Writer writer = new StringWriter();
        try {
            template.evaluate(writer, context);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String htmlContent = writer.toString();

        return Email.builder()
                .sender(sender)
                .subject(subject)
                .htmlContent(htmlContent)
                .build();
    }

    public Email buildNewsletterEmail(String locationName, List<Product> products) {
        String subject = "New products found on IKEA " + locationName + " website";

        PebbleTemplate template = pebbleEngine.getTemplate("email/newsletter");

        Map<String, Object> context = new HashMap<>();
        context.put("locationName", locationName);
        context.put("products", products);
        context.put("unsubscribeUrl", "{{ unsubscribe_url }}"); // will be replaced by email provider on a per-user basis

        Writer writer = new StringWriter();
        try {
            template.evaluate(writer, context);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String htmlContent = writer.toString();

        return Email.builder()
                .sender(sender)
                .subject(subject)
                .htmlContent(htmlContent)
                .build();
    }
}
