package com.httpain.meatballs.infrastructure.storage;

import com.httpain.meatballs.model.Location;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class LocationRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public LocationRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Location findById(long id) {
        return jdbcTemplate.queryForObject("SELECT id, name, hostname, new_items_path " +
                        "FROM location WHERE id = :id",
                new MapSqlParameterSource("id", id),
                new LocationRowMapper()
        );
    }

    public List<Location> findAll() {
        return jdbcTemplate.query(
                "SELECT id, name, hostname, new_items_path FROM location",
                new LocationRowMapper()
        );
    }


    private static class LocationRowMapper implements RowMapper<Location> {
        @Override
        public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Location(
                    rs.getInt("id"),
                    rs.getString("name").trim(),
                    rs.getString("hostname"),
                    rs.getString("new_items_path"));
        }
    }

}
