package com.httpain.meatballs.model;

import lombok.Value;

@Value
public class NewsletterSubscriber {
    String email;
    String unsubscribeLink;
}
