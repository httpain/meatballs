package com.httpain.meatballs.model;

import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Value
public class Subscription {

    private static final String SEPARATOR = "::";

    @Pattern(regexp = ".+@.+\\..+")
    private final String email;

    //TODO validate existing locationId
    @Min(1)
    private final long locationId;

    public String toIdentity() {
        return email + SEPARATOR + locationId;
    }

    public static Subscription parseIdentity(String identity) {
        int separatorStart = identity.lastIndexOf(SEPARATOR);
        String email = identity.substring(0, separatorStart);
        long locationId = Long.valueOf(identity.substring(separatorStart + SEPARATOR.length()));
        return new Subscription(email, locationId);
    }

}
