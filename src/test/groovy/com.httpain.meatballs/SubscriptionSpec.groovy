package com.httpain.meatballs

import com.github.tomakehurst.wiremock.WireMockServer
import com.httpain.meatballs.infrastructure.crypto.HardcodedCrypto
import com.httpain.meatballs.model.Subscription
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.util.UriComponentsBuilder
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.WireMock.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = [Application.class, HardcodedCrypto.class])
@ActiveProfiles("test")
class SubscriptionSpec extends Specification {

    @LocalServerPort
    int port

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate

    @Autowired
    TestRestTemplate restTemplate

    WireMockServer wireMockServer

    HardcodedCrypto crypto = new HardcodedCrypto()

    private static final String VALID_RESPONSE = """ { "results": { "total_rejected_recipients": 0, "total_accepted_recipients": 1, "id": "1234567890" } } """
    private static final String EMAIL = 'foo@example.co.uk'

    private static final long LOCATION_ID = 1

    Subscription subscription = new Subscription(EMAIL, LOCATION_ID)

    def setup() {
        // workaround because normal @Rule doesn't work on custom port, needs calling configureFor
        wireMockServer = new WireMockServer(8189)
        wireMockServer.start()
        configureFor(wireMockServer.port())
    }

    def cleanup() {
        wireMockServer.stop()
    }

    def "should send confirmation email on subscription form submit"() {
        given: "a mock SparkPost server set up"
        wireMockServer.stubFor(post(urlPathEqualTo("/transmissions/"))
                .willReturn(aResponse().withStatus(200).withBody(VALID_RESPONSE)))

        when: "user submits subscription form"
        def response = restTemplate.postForEntity("http://localhost:$port/subscribe",
                prepareForm([
                        locationId: LOCATION_ID.toString(),
                        email     : EMAIL
                ]), String.class)

        then: "user is redirected to 'Subscribed' page"
        response.statusCodeValue == 302
        response.headers['Location'][0].contains("/subscribed")

        then: "a request to Sparkpost is sent"
        verify(postRequestedFor(urlPathEqualTo("/transmissions/")))

        then: "a request to Sparkpost contains valid data"
        def requestBody = getAllServeEvents()[0].request.bodyAsString
        def json = new JsonSlurper()
        def jsonBody = json.parseText(requestBody)
        jsonBody['options']['open_tracking'] == false
        jsonBody['options']['click_tracking'] == false
        jsonBody['recipients'].size() == 1
        jsonBody['recipients'][0]['address']['email'] == EMAIL
        jsonBody['recipients'][0]['substitution_data'] == [:]
        jsonBody['content']['from'] == 'Test Meatballs <meatballs@mail.httpain.com>'
        jsonBody['content']['subject'].toLowerCase().contains('confirm')

        then: "confirmation link contains correct encrypted/encoded tokens"
        def xml = new XmlSlurper().parseText(jsonBody['content']['html'] as String)
        def confirmationUrl = xml['a'].@href as String
        def queryParams = UriComponentsBuilder.fromUriString(confirmationUrl).build().queryParams
        def encryptedIdentity = queryParams['i'][0]
        def confirmationToken = queryParams['t'][0]

        crypto.identityEncryptor().decrypt(encryptedIdentity) == subscription.toIdentity()
        crypto.subscribeTokenEncoder().matches(subscription.toIdentity(), confirmationToken)
    }

    def "should redirect from subscription page to 'Already subscribed' page when subscriber is already present in database"() {
        given: "a prefilled database with a subscriber"
        jdbcTemplate.update("INSERT INTO subscription (email, location_id) " +
                "VALUES (:email, :locationId)",
                new BeanPropertySqlParameterSource(subscription))

        when: "user submits subscription form with existing pair of email and location id"
        def response = restTemplate.postForEntity("http://localhost:$port/subscribe",
                prepareForm([
                        locationId: LOCATION_ID.toString(),
                        email     : EMAIL
                ]), String.class)

        then: "user is redirected to 'Already subscribed' page"
        response.statusCodeValue == 302
        response.headers['Location'][0].contains("/already_subscribed")
    }

    def "should redirect back to main page if email is invalid"() {
        when: "user submits subscription form with invalid email, bypassing browser validation"
        def response = restTemplate.postForEntity("http://localhost:$port/subscribe",
                prepareForm([
                        locationId: LOCATION_ID.toString(),
                        email     : "nonsense@123"
                ]), String.class)

        then: "user is redirected back to main page"
        response.statusCodeValue == 302
//        response.headers['Location'][0].contains("") TODO check URL
    }

    //TODO rewrite to HttpClient to use the same stuff as ConfirmationSpec?
    def prepareForm(Map<String, String> fields) {
        HttpHeaders headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>()
        fields.each { key, value -> map.add(key, value) }

        return new HttpEntity<MultiValueMap<String, String>>(map, headers)
    }

}