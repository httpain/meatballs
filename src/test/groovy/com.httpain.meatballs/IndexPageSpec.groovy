package com.httpain.meatballs


import org.jsoup.Jsoup
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
class IndexPageSpec extends Specification {

    @LocalServerPort
    int port

    def "should have more than one location in a dropdown"() {
        given:
        def document = Jsoup.connect("http://localhost:$port/").get()
        def locations = document.body().select("form option")

        expect:
        locations.size() > 1
    }

    def "first option in a location dropdown should be a disabled placeholder"() {
        given:
        def document = Jsoup.connect("http://localhost:$port/").get()
        def locations = document.body().select("form option")

        expect:
        locations[0].hasAttr("disabled")
        locations[0].text().toLowerCase().contains("select")
    }


}
