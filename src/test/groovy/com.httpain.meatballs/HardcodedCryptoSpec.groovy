package com.httpain.meatballs

import com.httpain.meatballs.infrastructure.crypto.HardcodedCrypto
import spock.lang.Specification

class HardcodedCryptoSpec extends Specification {

    def "should get initial value after encrypting and decrypting"() {
        expect: "values are same even if calling different instances"
        new HardcodedCrypto().identityEncryptor().decrypt(
                new HardcodedCrypto().identityEncryptor().encrypt("foo")
        ) == "foo"
    }

    def "should match encoded token"() {
        expect: "values match even if calling different instances"
        new HardcodedCrypto().subscribeTokenEncoder().matches(
                "foo",
                new HardcodedCrypto().subscribeTokenEncoder().encode("foo")
        )
    }

}